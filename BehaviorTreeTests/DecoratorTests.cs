﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using BehaviorTreeLibrary;

namespace BehaviorTreeTests
{
    public class DecoratorTests
    {
        [Test]
        public void Tick_ConditionTrue_RunChild()
        {
            int health = 10;
            Decorator decorator = new Decorator();
            MockBehavior behavior = decorator.Add<MockBehavior>();

            decorator._canRun = () =>
            {
                if (health < 50)
                {
                    return true;
                }

                return false;
            };

            decorator.Tick();
            Assert.AreEqual(Status.BhRunning, behavior.Status);
        }

        [Test]
        public void Tick_ConditionFalse_ReturnValue()
        {
            int health = 60;
            Decorator decorator = new Decorator();
            MockBehavior behavior = decorator.Add<MockBehavior>();
            decorator.ReturnStatus = Status.BhSuccess;

            decorator._canRun = () =>
            {
                if (health < 50)
                {
                    return true;
                }

                return false;
            };

            decorator.Tick();
            Assert.AreEqual(Status.BhInvalid, behavior.Status);
            Assert.AreEqual(Status.BhSuccess, decorator.Status);
        }
    }
}
